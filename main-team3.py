import copy

nOfPhoto = 0
photos = []

slideshow = []

class Tag:
    def __init__(self, tag):
        self.tag = tag

    def __eq__(self, other):
        return self.tag == other.tag

    def __repr__(self):
        return str(self.tag)

class Photo:
    def __init__(self, id, o, n):
        self.id = id
        self.orientation = o
        self.nOfTags = n
        self.tags = []

    def __repr__(self):
        strTags = ""
        for tag in self.tags:
            strTags+=str(tag)+" "
        return str(self.orientation)+" "+str(self.nOfTags)+" "+strTags

class Slide:
    def __init__(self):
        self.photos = []

    def __repr__(self):
        strTags = ""
        index = 0
        strTags += "----------------------------------\n"
        for photo in self.photos:
            strTags += "Photo "+str(index)+": "
            for tag in photo.tags:
                strTags+=str(tag)+" "
            index += 1
            strTags += "\n"
        strTags += "----------------------------------\n"

        return strTags

def createOutput():
    global slideshow
    outputFile = "output/output_example.out"
    file = open(outputFile, "w")
    file.write(str(len(slideshow))+"\n")
    for slide in slideshow:
        for photo in slide.photos:
            file.write(str(str(photo.id)+" "))
        file.write("\n")
    file.close()


slideV = Slide()
slideH = Slide()

filename = "input/a_example.txt"
textfile = open(filename, "r").read()
textfile = textfile.split('\n')
firstRow = textfile[0]
nOfPhoto = int(firstRow)
for line in range(1, len(textfile)-1):
    cells = textfile[line].split(" ")
    orientation = str(cells[0])
    nOftags = str(cells[1])
    photo = Photo(line-1, orientation, nOftags)
    for cell in range(2, len(cells)):
        tag = Tag(str(cells[cell]))
        photo.tags.append(tag)
    photos.append(photo)

for photo in photos:
    #print photo
    if photo.orientation == 'H':
        slideH.photos.append(photo)
        slideshow.append(slideH)
        slideH = Slide()
    else:
        slideV.photos.append(photo)
        if len(slideV.photos) == 2:
            slideshow.append(slideV)
            slideV = Slide()

for slide in slideshow:
    print slide

createOutput()





